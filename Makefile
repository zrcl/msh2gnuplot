#about
    # msh2gnuplot
    # Copyright (C) 2021  Christophe Langlois

    # This program is free software: you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation, either version 3 of the License, or
    # (at your option) any later version.

    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.

    # You should have received a copy of the GNU General Public License
    # along with this program.  If not, see <http://www.gnu.org/licenses/>.

    # msh2gnuplot  Copyright (C) 2021  Christophe Langlois
    # This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    # This is free software, and you are welcome to redistribute it
    # under certain conditions; type `show c' for details.

    #  _____ _____ _____ _____ _____ _____ _____ __    
    # |__   |   __| __  |     |     |     |     |  |   
    # |   __|   __|    -|  |  |   --|  |  |  |  |  |__ 
    # |_____|_____|__|__|_____|_____|_____|_____|_____|

install:
	cp src/msh2gnuplot.def msh2gnuplot
	@echo "printf \"%s\n\" \$$inFile \$$tag \$$out \$$wire | python "$(shell pwd)"/src/convert.py >> /dev/null" >> msh2gnuplot
	sudo cp msh2gnuplot /usr/bin/msh2gnuplot
uninstall:
	rm msh2gnuplot
	sudo rm /usr/bin/msh2gnuplot
