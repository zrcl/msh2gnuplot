#about
# msh2gnuplot
# Copyright (C) 2021  Christophe Langlois

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# msh2gnuplot  Copyright (C) 2021  Christophe Langlois
# This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
# This is free software, and you are welcome to redistribute it
# under certain conditions; type `show c' for details.

#  _____ _____ _____ _____ _____ _____ _____ __    
# |__   |   __| __  |     |     |     |     |  |   
# |   __|   __|    -|  |  |   --|  |  |  |  |  |__ 
# |_____|_____|__|__|_____|_____|_____|_____|_____|
import numpy as np
import order
if __name__=='__main__':
	print('give path to .msh:')
	meshName = input()
	print('give the physical tag of the domain to export:')
	tag = int(input())
	print('give path to result .dat file :')
	foutname = input()
	print('Wireframe ? (1 or 0) :')
	wire = int(input())
	if wire==1:
		wireFrame=True
	else:
		wireFrame=False
	#tag=1
	nDecimals=5
	#meshName='test.msh'
	fid=open(meshName,'r')
	line=fid.readline()
	line=fid.readline()
	if line[:3]!='2.2':
		raise TypeError('This program works with file formats from version 2.2, try adding /Mesh.MshFileVersion=2;/ at the top of your .geo and remesh')
	line=fid.readline()
	line=fid.readline()
	if line=='$PhysicalNames\n':
		nPhysicalName=int(fid.readline())
		mapPhysicalName=dict()
		for _ in range(nPhysicalName):
			line=fid.readline()
			mapPhysicalName[line.split()[2]]=line.split()[1]
		line=fid.readline()
		line=fid.readline()
	if line=='$Nodes\n':
		nNodes=int(fid.readline())
		coords=[[0.,0.,0.]]*nNodes
		for node in range(nNodes):
			line=fid.readline()
			coords[node]=[float(k) for k in line.split()[1:]]
	line=fid.readline()
	line=fid.readline()
	if line=='$Elements\n':
		nElts=int(fid.readline())
		eltsType=np.zeros((nElts,),dtype=int)
		physicalTag=np.zeros((nElts,),dtype=int)
		elementaryTag=np.zeros((nElts,),dtype=int)
		connect=[None]*nElts
		for elt in range(nElts):
			line=fid.readline().split()
			eltsType[elt]=int(line[1])
			physicalTag[elt]=int(line[3])
			elementaryTag[elt]=int(line[4])
			connect[elt]=list(map(int,line[int(line[2])+3:]))
	fid.close()
	mask=np.argwhere(physicalTag==tag).flatten().tolist()
	if wireFrame:
		patterns=order.wireFrame()
		edges=[]
		for eltId in mask:
			elt=connect[eltId]
			localPattern=patterns[eltsType[eltId]]
			for localEdge in localPattern:
				edges+=[[elt[k]-1 for k in localEdge]]
		edges=np.array(edges)
		edges=np.sort(edges)
		edges=np.unique(edges,axis=0)
		fid=open(foutname,'w')
		for edge in edges.tolist():
			node=edge[0]
			fid.write('\t'.join([('%.'+str(nDecimals)+'f')%k for k in coords[node]])+'\n')
			node=edge[1]
			fid.write('\t'.join([('%.'+str(nDecimals)+'f')%k for k in coords[node]])+'\n')
			fid.write('\n\n')
		fid.close()
	else:
		patterns=order.faces()
		fid=open(foutname,'w')
		for eltId in mask:
			elt=connect[eltId]
			pattern=patterns[eltsType[eltId]]
			for subPattern in pattern:
				globalPattern=[elt[k]-1 for k in subPattern]
				for node in globalPattern:
					fid.write('\t'.join([('%.'+str(nDecimals)+'f')%k for k in coords[node]])+'\n')
				fid.write('\n\n')
		fid.close()
