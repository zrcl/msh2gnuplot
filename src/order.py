def faces():
	"""
	to generate gnuplots with meshes with solid faces
	"""
	dic={}
	#2 nodes line
	dic[1]=[[0,1]]
	#3 nodes trangle
	dic[2]=[[0,1,2,0]]
	#4 nodes quadrangle
	dic[3]=[[0,1,2,3,0]]
	#4 nodes tetrahedron
	dic[4]=[[0,1,3,0],[0,2,3,0],[0,1,2,0],[1,2,3,0]]
	#8 nodes hexahedron
	dic[5]=[[0,4,7,3,0],[1,5,6,2,1],[0,1,5,4,0],[4,5,6,7,4],[7,6,2,3,7],[3,2,1,0,3]]
	#6 nodes prism
	dic[6]=[[0,1,2,0],[3,4,5,3],[0,1,4,3,0],[0,2,5,3,0],[1,2,5,4,1]]
	#5 nodes pyramid
	dic[7]=[[0,1,2,3,0],[0,4,1,0],[4,1,2,4],[4,2,3,4],[4,3,0,4]]
	#3 nodes line
	dic[8]=[[0,2,1]]
	#6 nodes triangle
	dic[9]=[[0,3,1,4,2,5,0]]
	#8 nodes quadrangle
	dic[16]=[[0,4,1,5,2,6,3,7,0]]
	#9-node second order quadrangle (4 nodes associated with the vertices, 4 with the edges and 1 with the face).
	dic[10]=dic[16]+[[8]]
	#10-nodes tetrahedron
	dic[11]=[[0,7,3,9,1,4,0],[0,7,3,8,2,6,0],[3,9,1,5,2,8,3],[1,5,2,6,0,4,1]]
	#20 nodes hexahedron
	dic[17]=[[0,8,1,11,2,13,3,9,0],[4,16,5,18,6,19,7,17,4],[0,10,4,16,5,12,1,8,0],[1,12,5,18,6,14,2,11,1],[2,14,6,19,7,15,3,13,2],[3,15,7,17,4,10,0,9,3]]
	return dic
def wireFrame():
	"""
	to generate gnuplots with wireframe meshes
	"""
	dic={}
	#2 nodes line
	dic[1]=[[0,1]]
	#3 nodes trangle
	dic[2]=[[0,1],[1,2],[2,0]]
	#4 nodes quadrangle
	dic[3]=[[0,1],[1,2],[2,3],[3,0]]
	#4 nodes tetrahedron
	dic[4]=[[0,1],[0,2],[0,3],[1,3],[2,1],[2,3]]
	#8 nodes hexahedron
	dic[5]=[[0,1],[1,2],[2,3],[3,0],[0,4],[4,7],[7,3],[1,5],[5,6],[6,2],[4,5],[6,7]]
	#6 nodes prism
	# dic[6]=[[0,1,2,0],[3,4,5,3],[0,1,4,3,0],[0,2,5,3,0],[1,2,5,4,1]]
	# #5 nodes pyramid
	# dic[7]=[[0,1,2,3,0],[0,4,1,0],[4,1,2,4],[4,2,3,4],[4,3,0,4]]
	# #3 nodes line
	# dic[8]=[[0,2,1]]
	# #6 nodes triangle
	# dic[9]=[[0,3,1,4,2,5,0]]
	# #8 nodes quadrangle
	# dic[16]=[[0,4,1,5,2,6,3,7,0]]
	# #9-node second order quadrangle (4 nodes associated with the vertices, 4 with the edges and 1 with the face).
	# dic[10]=dic[16]+[[8]]
	# #10-nodes tetrahedron
	# dic[11]=[[0,7,3,9,1,4,0],[0,7,3,8,2,6,0],[3,9,1,5,2,8,3],[1,5,2,6,0,4,1]]
	# #20 nodes hexahedron
	# dic[17]=[[0,8,1,11,2,13,3,9,0],[4,16,5,18,6,19,7,17,4],[0,10,4,16,5,12,1,8,0],[1,12,5,18,6,14,2,11,1],[2,14,6,19,7,15,3,13,2],[3,15,7,17,4,10,0,9,3]]
	return dic
