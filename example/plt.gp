# uncomment these two line and comment the next to obtain beautiful latex understandle vectorial figues.
# set terminal cairolatex size 6cm,6cm
# set output 'test.tex'
set terminal wxt

set pm3d depthorder border lc "black"

set view equal xyz

unset ztics
unset key
set xlabel 'x(m)'
set ylabel 'y(m)'

splot 'layer1.dat' with polygons lc 'blue','layer2.dat' with polygons lc 'green','skin.dat' with polygons lc 'red'
