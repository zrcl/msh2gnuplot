Mesh.ElementOrder=1;
Mesh.MshFileVersion=2;
//Mesh.SecondOrderIncomplete=1;

lx=.5;
ly=.8;
e1=0.01;
e2=0.005;

t=.05;

Point(1)={0,0,0,t};
Point(2)={lx,0,0,t};
Point(3)={lx,ly,0,t};
Point(4)={0,ly,0,t};

Line(1)={1,2};
Line(2)={2,3};
Line(3)={3,4};
Line(4)={4,1};

Line Loop(10)={1,2,3,4};
Plane Surface(20)=10;

// Transfinite Line{1,2,3,4} = 15;
Transfinite Surface{20};
Recombine Surface{20};

newEntities[] = 
Extrude { 0,0,e1 }
{
	Surface{20};
	Layers{1};
	Recombine;
};

newEntities2[] = 
Extrude { 0,0,e2 }
{
	Surface{42};
	Layers{1};
	Recombine;
};

Physical Volume(1)={1};
Physical Volume(2)={2};
Physical Surface(12)={20};
